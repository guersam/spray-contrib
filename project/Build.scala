import sbt._
import Keys._

object Build extends Build {
  import BuildSettings._
  import Dependencies._

  lazy val root = Project("spray-contrib", file("."))
    .aggregate(data, session, common, examples)
    .settings(rootSettings: _*)

  lazy val data = Project("spray-data", file("spray-data"))
    .dependsOn(common)
    .settings(moduleSettings: _*)
    .settings(libraryDependencies ++=
      provided(akkaActor, sprayJson) ++
      compile(sprayRouting) ++
      test(scalaTest, akkaTestkit, sprayTestkit))

  lazy val session = Project("spray-session", file("spray-session"))
    .dependsOn(common)
    .settings(moduleSettings: _*)
    .settings(libraryDependencies ++=
      provided(akkaActor, pickling) ++
      compile(sprayRouting, commonsCodec, quasiquotes) ++
      test(scalaTest, akkaTestkit, sprayTestkit))

  lazy val common = Project("spray-common", file("spray-common"))
    .settings(moduleSettings: _*)
    .settings(libraryDependencies ++=
      provided(akkaActor, sprayRouting, sprayJson) ++
      test(scalaTest, akkaTestkit, sprayTestkit))

  lazy val examples = Project("examples", file("examples"))
    .aggregate(dataExamples, sessionExamples)
    .settings(exampleSettings: _*)

  lazy val dataExamples = Project("spray-data-examples", file("examples/spray-data"))
    .dependsOn(common, data)
    .settings(exampleSettings: _*)
    .settings(libraryDependencies ++=
      compile(akkaActor, sprayCan, sprayJson) ++
      runtime(akkaSlf4j, logback))

  lazy val sessionExamples = Project("spray-session-examples", file("examples/spray-session"))
    .dependsOn(session)
    .settings(exampleSettings: _*)
    .settings(libraryDependencies ++=
      compile(akkaActor, sprayCan) ++
      runtime(akkaSlf4j, logback))
}

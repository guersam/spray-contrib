import sbt._
import Keys._
import com.typesafe.sbt.SbtScalariform
import com.typesafe.sbt.SbtScalariform.ScalariformKeys

object BuildSettings {
  val Version = "0.1.0-SNAPSHOT"

  lazy val basicSettings = Defaults.defaultSettings ++ Seq(
    organization := "com.binarycamp.spray-contrib",
    version := Version,
    scalaVersion := "2.10.3",
    resolvers ++= Dependencies.repositories,
    scalacOptions := Seq(
      "-encoding", "utf8",
      "-feature",
      "-unchecked",
      "-deprecation",
      "-target:jvm-1.6",
      "-language:_",
      "-Xlog-reflective-calls"))

  lazy val rootSettings = basicSettings ++ noPublishing

  lazy val moduleSettings = basicSettings ++ formatSettings ++ Seq(crossPaths := false)

  lazy val exampleSettings = basicSettings ++ formatSettings ++ noPublishing

  lazy val formatSettings = SbtScalariform.scalariformSettings ++ Seq(
    ScalariformKeys.preferences in Compile := formattingPreferences,
    ScalariformKeys.preferences in Test := formattingPreferences)

  import scalariform.formatter.preferences._

  lazy val formattingPreferences = FormattingPreferences()
    .setPreference(RewriteArrowSymbols, true)
    .setPreference(AlignParameters, true)
    .setPreference(AlignSingleLineCaseStatements, true)
    .setPreference(DoubleIndentClassDeclaration, true)

  lazy val noPublishing = Seq(
    publish := (),
    publishLocal := ())
}

import sbt._

object Dependencies {
  val repositories = Seq(
    Resolver.sonatypeRepo("snapshots"),
    "spray repo" at "http://repo.spray.io")

  def compile   (deps: ModuleID*): Seq[ModuleID] = deps map (_ % "compile")
  def provided  (deps: ModuleID*): Seq[ModuleID] = deps map (_ % "provided")
  def test      (deps: ModuleID*): Seq[ModuleID] = deps map (_ % "test")
  def runtime   (deps: ModuleID*): Seq[ModuleID] = deps map (_ % "runtime")
  def container (deps: ModuleID*): Seq[ModuleID] = deps map (_ % "container")

  val akkaActor    = "com.typesafe.akka" %% "akka-actor"      % "2.3.0"
  val akkaSlf4j    = "com.typesafe.akka" %% "akka-slf4j"      % "2.3.0"
  val logback      = "ch.qos.logback"    %  "logback-classic" % "1.0.13"
  val pickling     = "org.scala-lang"    %% "scala-pickling"  % "0.8.0-SNAPSHOT"
  val sprayCan     = "io.spray"          %  "spray-can"       % "1.3.1"
  val sprayHttp    = "io.spray"          %  "spray-http"      % "1.3.1"
  val sprayRouting = "io.spray"          %  "spray-routing"   % "1.3.1"
  val sprayJson    = "io.spray"          %% "spray-json"      % "1.2.5"
  val commonsCodec = "commons-codec"     %  "commons-codec"   % "1.8"
  val quasiquotes  = "org.scalamacros"   %% "quasiquotes"     % "2.0.0-M8"
  val scalaTest    = "org.scalatest"     %% "scalatest"       % "2.1.0"
  val akkaTestkit  = "com.typesafe.akka" %% "akka-testkit"    % "2.3.0"
  val sprayTestkit = "io.spray"          %  "spray-testkit"   % "1.3.1"
}

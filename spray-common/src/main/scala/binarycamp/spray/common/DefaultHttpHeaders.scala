package binarycamp.spray.common

import spray.http.CacheDirectives._
import spray.http.HttpHeader
import spray.http.HttpHeaders._

object DefaultHttpHeaders {
  val NoCacheHeaders: List[HttpHeader] = `Cache-Control`(`no-store`) :: RawHeader("Pragma", "no-cache") :: Nil
}

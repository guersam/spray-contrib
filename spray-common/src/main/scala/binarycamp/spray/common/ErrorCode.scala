package binarycamp.spray.common

import java.util.concurrent.atomic.AtomicReference
import scala.annotation.tailrec
import ErrorCode.Tag

sealed trait ErrorCode {
  def id: String

  def parent: Option[ErrorCode]

  def hasParent: Boolean = parent.isDefined

  def hasTag(tag: Tag): Boolean
}

object ErrorCode {
  trait Tag

  def unapply(error: Error): Option[ErrorCode] = Some(error.code)
}

private[common] case class ErrorCodeImpl(id: String, parent: Option[ErrorCode], tags: Set[Tag]) extends ErrorCode {
  require(parent.isEmpty || !parent.get.hasParent, s"Parent ${parent.get.id} is not a top-level ErrorCode")

  override def hasTag(tag: Tag): Boolean = tags.contains(tag)
}

final case class ErrorCodeBuilder(id: String, parent: Option[ErrorCode], tags: Set[Tag]) {
  def childOf(parent: ErrorCode): ErrorCodeBuilder = copy(parent = Some(parent))

  def withTag(tag: Tag): ErrorCodeBuilder = copy(tags = tags + tag)

  def withTags(tags: Tag*): ErrorCodeBuilder = copy(tags = this.tags ++ tags)

  private[common] def build: ErrorCode = ErrorCodeImpl(id, parent, tags)
}

object ErrorCodeBuilder extends ToErrorCodeBuilderPimps

trait ToErrorCodeBuilderPimps {
  implicit def stringToErrorCodeBuilder(id: String): ErrorCodeBuilder = ErrorCodeBuilder(id, None, Set())
  implicit def symbolToErrorCodeBuilder(symbol: Symbol): ErrorCodeBuilder = ErrorCodeBuilder(symbol.name, None, Set())
}

trait ErrorCodeRegistry extends ToErrorCodeBuilderPimps {
  final def errorCode(builder: ErrorCodeBuilder): ErrorCode = register(builder.build)

  private[this] val registry = new AtomicReference(Map.empty[String, ErrorCode])

  @tailrec
  private def register(code: ErrorCode): ErrorCode = {
    val original = registry.get()
    require(!original.contains(code.id), s"ErrorCode ${code.id} has already been registered")
    val updated = original.updated(code.id, code)
    if (registry.compareAndSet(original, updated)) code else register(code)
  }
}

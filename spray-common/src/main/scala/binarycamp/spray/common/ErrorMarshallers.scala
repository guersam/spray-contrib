package binarycamp.spray.common

import spray.httpx.marshalling.{ Marshaller, ToResponseMarshaller }
import DefaultHttpHeaders.NoCacheHeaders

trait ErrorMarshallers {
  implicit def errorMarshaller[T](implicit translator: ErrorTranslator[T],
                                  errorToStatusCode: ErrorToStatusCodeMapper,
                                  m: Marshaller[T]): ToResponseMarshaller[Error] =
    ToResponseMarshaller.fromStatusCodeAndHeadersAndT.compose {
      error ⇒ (errorToStatusCode(error), NoCacheHeaders, translator(error))
    }

  implicit def errorLocalizableMarshaller[T](implicit translator: ErrorTranslator[T],
                                             errorToStatusCode: ErrorToStatusCodeMapper,
                                             m: Marshaller[T]): LocalizableMarshaller[Error] =
    LocalizableMarshaller[Error] { languages ⇒
      ToResponseMarshaller.fromStatusCodeAndHeadersAndT.compose {
        error ⇒ (errorToStatusCode(error), NoCacheHeaders, translator(error, languages))
      }
    }
}

object ErrorMarshallers extends ErrorMarshallers

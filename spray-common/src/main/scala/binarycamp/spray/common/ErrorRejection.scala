package binarycamp.spray.common

import spray.routing.Rejection

case class ErrorRejection(error: Error) extends Rejection

object ErrorRejection {
  def apply(code: ErrorCode, args: Any*): ErrorRejection = ErrorRejection(Error(code, None, args))
}

package binarycamp.spray.common

import spray.http.StatusCode

class PimpedResult[T](val result: Result[T]) extends AnyVal {
  def withStatusCode(sc: StatusCode): Result[(StatusCode, T)] = result.right.map(t ⇒ (sc, t))
}

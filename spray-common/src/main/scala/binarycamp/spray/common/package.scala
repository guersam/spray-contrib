package binarycamp.spray

package object common {
  type Result[T] = Either[Error, T]

  implicit def pimpResult[T](result: Result[T]): PimpedResult[T] = new PimpedResult[T](result)
}

package binarycamp.spray.pimp

import com.typesafe.config.Config
import spray.http.Uri.Path

class PimpedConfig(val config: Config) extends AnyVal {
  def getOptConfig(path: String): Option[Config] =
    if (config.hasPath(path)) Some(config.getConfig(path)) else None

  def getOptString(path: String): Option[String] =
    if (config.hasPath(path)) Some(config.getString(path)) else None

  def getOptLong(path: String): Option[Long] =
    if (config.hasPath(path)) Some(config.getLong(path)) else None

  def getUriPath(path: String): Path = Path(config.getString(path))
}

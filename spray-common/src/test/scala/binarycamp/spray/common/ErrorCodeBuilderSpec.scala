package binarycamp.spray.common

import org.scalatest.FlatSpec
import org.scalatest.Matchers._
import ErrorCodes._

class ErrorCodeBuilderSpec extends FlatSpec {
  "ErrorCodeBuilder" should "build an ErrorCode with the same id, parent and categories" in {
    ErrorCodeBuilder("CodeWithTags", None, Set(Tag1, Tag2)).build should be(CodeWithTags)
    ErrorCodeBuilder("CodeWithParentAndTags", Some(CodeWithoutParent), Set(Tag1, Tag2)).build should be(CodeWithParentAndTags)
  }
}

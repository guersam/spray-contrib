package binarycamp.spray.common

import org.scalatest.FlatSpec
import org.scalatest.Matchers._
import ErrorCodes._

class ErrorCodeImplSpec extends FlatSpec {
  "ErrorCodeImpl" should "produce IllegalArgumentException when parent is not a top-level ErrorCode" in {
    intercept[IllegalArgumentException] {
      ErrorCodeImpl("code", Some(CodeWithParent), Set())
    }
  }

  it should "return true on hasParent when parent is non-empty" in {
    CodeWithParent.hasParent should be(true)
  }

  it should "return false on hasParent when parent is empty" in {
    CodeWithoutParent.hasParent should be(false)
  }

  it should "return true on hasTag for all its tags" in {
    CodeWithTags.hasTag(Tag1) should be(true)
    CodeWithTags.hasTag(Tag2) should be(true)
  }

  it should "return false on hasTag for other tags" in {
    CodeWithTags.hasTag(Tag3) should be(false)
  }
}


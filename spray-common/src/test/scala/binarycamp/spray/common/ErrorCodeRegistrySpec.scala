package binarycamp.spray.common

import org.scalatest.FlatSpec

class ErrorCodeRegistrySpec extends FlatSpec {
  "ErrorCodeRegistry" should "produce IllegalArgumentException when the same code is registered twice" in {
    val registry = new ErrorCodeRegistry {}
    registry.errorCode("CodeWithoutParent")
    intercept[IllegalArgumentException] {
      registry.errorCode("CodeWithoutParent")
    }
  }
}

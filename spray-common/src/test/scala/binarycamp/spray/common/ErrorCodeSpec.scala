package binarycamp.spray.common

import org.scalatest.FlatSpec
import Errors._

class ErrorCodeSpec extends FlatSpec {
  "ErrorCode" should "extract ErrorCode from an Error" in {
    Error1 match {
      case ErrorCode(ErrorCode1) ⇒
      case _                     ⇒ fail("failed to extract ErrorCode")
    }
  }
}

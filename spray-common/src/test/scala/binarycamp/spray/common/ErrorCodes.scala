package binarycamp.spray.common

object ErrorCodes {
  val CodeWithoutParent = ErrorCodeImpl("CodeWithoutParent", None, Set())
  val CodeWithParent = ErrorCodeImpl("CodeWithParent", Some(CodeWithoutParent), Set())
  val CodeWithTags = ErrorCodeImpl("CodeWithTags", None, Set(Tag1, Tag2))
  val CodeWithParentAndTags = ErrorCodeImpl("CodeWithParentAndTags", Some(CodeWithoutParent), Set(Tag1, Tag2))

  case object Tag1 extends ErrorCode.Tag
  case object Tag2 extends ErrorCode.Tag
  case object Tag3 extends ErrorCode.Tag
}

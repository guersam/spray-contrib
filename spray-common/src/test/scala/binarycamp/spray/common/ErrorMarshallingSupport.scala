package binarycamp.spray.common

import spray.http.LanguageRange
import Errors._

trait ErrorMarshallingSupport {
  implicit val TestErrorTranslator = new ErrorTranslator[String] {
    override def apply(error: Error, languages: Seq[LanguageRange]): String = error.code match {
      case ErrorCode1 if languages.exists(_.matches(DE)) ⇒ Error1Message_DE
      case ErrorCode1                                    ⇒ Error1Message_EN
      case ErrorCode2 if languages.exists(_.matches(DE)) ⇒ Error2Message_DE
      case ErrorCode2                                    ⇒ Error2Message_EN
    }
  }

  implicit val TestStatusCodeMapper = ErrorToStatusCodeMapper {
    case ErrorCode(ErrorCode1) ⇒ Error1StatusCode
    case ErrorCode(ErrorCode2) ⇒ Error2StatusCode
  }

  def result(error: Error): Result[String] = Left(error)
}

package binarycamp.spray.data

import spray.httpx.unmarshalling._
import spray.routing._
import spray.routing.Directives._
import PageRequest._
import PageRequestDeserializers._

trait PageDirectives {
  def getWithPageRequest: Directive1[Option[PageRequest]] = PageDirectives._getWithPageRequest
}

object PageDirectives extends PageDirectives {
  private val _getWithPageRequest: Directive1[Option[PageRequest]] = get & pageRequest

  private def pageRequest: Directive1[Option[PageRequest]] =
    parameterMultiMap.flatMap { params ⇒
      val parsed = for {
        page ← parse[String, Int](IndexParam, params.get(IndexParam).flatMap(_.headOption)).right
        size ← parse[String, Int](SizeParam, params.get(SizeParam).flatMap(_.headOption)).right
        sort ← parse[Seq[String], Sort](SortParam, params.get(SortParam).map(_.reverse)).right
        filter ← parse[String, Filter](FilterParam, params.get(FilterParam).flatMap(_.headOption)).right
      } yield (page, size, sort, filter)

      parsed match {
        case Right((None, None, None, None))   ⇒ provide(None)
        case Right((page, size, sort, filter)) ⇒ provide(Some(PageRequest(page.getOrElse(0), size, sort, filter)))
        case Left(rejection)                   ⇒ reject(rejection)
      }
    }

  private def parse[A, B](name: String, value: Option[A])(implicit d: Deserializer[Option[A], Option[B]]): Either[Rejection, Option[B]] =
    d(value).left.map {
      case MalformedContent(error, cause) ⇒ MalformedQueryParamRejection(name, error, cause)
      case error                          ⇒ throw new IllegalStateException(error.toString)
    }
}

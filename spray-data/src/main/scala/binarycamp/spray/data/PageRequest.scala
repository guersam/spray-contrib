package binarycamp.spray.data

case class PageRequest(index: Int, size: Option[Int], sort: Option[Sort], filter: Option[Filter])

object PageRequest {
  // format: OFF
  val IndexParam  = "page"
  val SizeParam   = "size"
  val SortParam   = "sort"
  val FilterParam = "filter"

  val AscParam    = "asc"
  val DescParam   = "desc"
}

package binarycamp.spray.data

import org.parboiled.errors.ParsingException
import spray.httpx.unmarshalling._
import PageRequestParsers._

private[data] trait PageRequestDeserializers {
  implicit object SortDeserializer extends Deserializer[Seq[String], Sort] {
    def apply(values: Seq[String]): Deserialized[Sort] = {
      try Right(parseSort(values))
      catch {
        case e: ParsingException ⇒ Left(MalformedContent(s"Values $values are not a valid Sort representation", e))
      }
    }
  }

  implicit object FilterDeserializer extends Deserializer[String, Filter] {
    def apply(value: String): Deserialized[Filter] = Right(Filter(value))
  }
}

private[data] object PageRequestDeserializers extends PageRequestDeserializers

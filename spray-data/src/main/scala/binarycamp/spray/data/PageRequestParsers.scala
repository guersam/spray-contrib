package binarycamp.spray.data

import org.parboiled.errors.{ ErrorUtils, ParsingException }
import org.parboiled.scala._
import scala.annotation.tailrec
import Order._
import PageRequest._

private[data] object PageRequestParsers extends Parser with CommonRules with SortRules {
  def parseSort(values: Seq[String]): Sort = {
    @tailrec
    def parseNext(values: Seq[String], acc: Sort): Sort = values match {
      case head :: tail ⇒ parseNext(tail, acc.merge(parse(sort, head)))
      case Nil          ⇒ acc
    }

    parseNext(values, Sort())
  }

  private def parse[T](rule: Rule1[T], value: String): T = {
    val parsingResult = ReportingParseRunner(rule).run(value)
    parsingResult.result.getOrElse {
      throw new ParsingException(ErrorUtils.printParseErrors(parsingResult))
    }
  }
}

private[data] trait CommonRules {
  this: Parser ⇒

  def fieldName = rule { group(letter ~ zeroOrMore(letter | digit)) ~> (_.toString) }

  def uppercase = rule { "A" - "Z" }

  def lowercase = rule { "a" - "z" }

  def letter = rule { uppercase | lowercase }

  def digit = rule { "0" - "9" }

  def word = rule { oneOrMore(letter | digit) ~> (_.toString) }
}

private[data] trait SortRules {
  this: Parser with CommonRules ⇒

  val sort = rule { oneOrMore(field, separator = ",") ~ EOI ~~> (Sort(_)) }

  def field = rule { fieldName ~ optional(":" ~ order) ~~> ((_, _)) }

  def order = rule { asc | desc }

  def asc = rule { ignoreCase(AscParam) ~ push(Asc) }

  def desc = rule { ignoreCase(DescParam) ~ push(Desc) }
}

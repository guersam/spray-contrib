package binarycamp.spray.data

import org.scalatest.FlatSpec
import org.scalatest.Matchers._
import spray.http.HttpMethods
import spray.routing.{ Directives, MethodRejection }
import spray.testkit.ScalatestRouteTest

class PageDirectivesSpec extends FlatSpec with ScalatestRouteTest with Directives with PageDirectives {
  "getWithPageRequest" should "reject non-GET requests" in {
    Seq(Post(), Put(), Patch(), Delete(), Options(), Head()).foreach {
      _ ~> getWithPageRequest { _ ⇒ complete("OK") } ~> check {
        rejection should be(MethodRejection(HttpMethods.GET))
      }
    }
  }

  def test[T](uri: String)(body: ⇒ T) =
    Get(uri) ~> getWithPageRequest { request ⇒ complete(request.toString()) } ~> check(body)

  it should "extract None when no page request parameter is present" in {
    test("?") {
      responseAs[String] should be("None")
    }
  }

  it should "extract PageRequest(1,None,None,None) for page=1" in {
    test("?page=1") {
      responseAs[String] should be("Some(PageRequest(1,None,None,None))")
    }
  }

  it should "extract PageRequest(0,2,None,None) for size=2" in {
    test("?size=2") {
      responseAs[String] should be("Some(PageRequest(0,Some(2),None,None))")
    }
  }

  it should "extract PageRequest(0,None,Sort(a->None),None) for sort=a" in {
    test("?sort=a") {
      responseAs[String] should be("Some(PageRequest(0,None,Some(Sort(List((a,None)))),None))")
    }
  }

  it should "extract PageRequest(0,None,Sort(a->asc),None) for sort=a:asc" in {
    test("?sort=a:asc") {
      responseAs[String] should be("Some(PageRequest(0,None,Some(Sort(List((a,Some(Asc))))),None))")
    }
  }

  it should "extract PageRequest(0,None,Sort(a->asc, b->desc),None) for sort=a:asc,b:desc" in {
    test("?sort=a:asc,b:desc") {
      responseAs[String] should be("Some(PageRequest(0,None,Some(Sort(List((a,Some(Asc)), (b,Some(Desc))))),None))")
    }
  }

  it should "extract PageRequest(0,None,Sort(a->asc, b->desc),None) for sort=a:asc&sort=b:desc" in {
    test("?sort=a:asc&sort=b:desc") {
      responseAs[String] should be("Some(PageRequest(0,None,Some(Sort(List((a,Some(Asc)), (b,Some(Desc))))),None))")
    }
  }

  it should "extract PageRequest(0,None,None,Filter(query)) for filter=query" in {
    test("?filter=query") {
      responseAs[String] should be("Some(PageRequest(0,None,None,Some(Filter(query))))")
    }
  }

  it should "extract PageRequest(1,2,None,None) for page=1&size=2" in {
    test("?page=1&size=2") {
      responseAs[String] should be("Some(PageRequest(1,Some(2),None,None))")
    }
  }

  it should "extract PageRequest(1,2,Sort(a->asc, b->desc),None) for page=1&size=2&sort=a:asc&sort=b:desc" in {
    test("?page=1&size=2&sort=a:asc&sort=b:desc") {
      responseAs[String] should be("Some(PageRequest(1,Some(2),Some(Sort(List((a,Some(Asc)), (b,Some(Desc))))),None))")
    }
  }

  it should "extract PageRequest(1,2,Sort(a->asc, b->desc),None) for page=1&size=2&sort=a:asc,b:desc" in {
    test("?page=1&size=2&sort=a:asc,b:desc") {
      responseAs[String] should be("Some(PageRequest(1,Some(2),Some(Sort(List((a,Some(Asc)), (b,Some(Desc))))),None))")
    }
  }

  it should "extract PageRequest(1,2,Sort(a->asc, b->desc),Filter(query)) for page=1&size=2&sort=a:asc,b:desc&filter=query" in {
    test("?page=1&size=2&sort=a:asc,b:desc&filter=query") {
      responseAs[String] should be("Some(PageRequest(1,Some(2),Some(Sort(List((a,Some(Asc)), (b,Some(Desc))))),Some(Filter(query))))")
    }
  }
}

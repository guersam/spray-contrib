package binarycamp.spray.data

import org.parboiled.errors.ParsingException
import org.scalatest.FlatSpec
import org.scalatest.Matchers._
import Order._

class PageRequestParsersSpec extends FlatSpec {
  "PageRequestParsers.parse" should "process a1" in {
    PageRequestParsers.parseSort(Seq("a1")) should be(Sort("a1" -> None))
  }

  it should "process an empty input" in {
    PageRequestParsers.parseSort(Seq.empty) should be(Sort())
  }

  it should "process a1:asc" in {
    PageRequestParsers.parseSort(Seq("a1:asc")) should be(Sort("a1" -> Some(Asc)))
  }

  it should "process a1:desc" in {
    PageRequestParsers.parseSort(Seq("a1:desc")) should be(Sort("a1" -> Some(Desc)))
  }

  it should "process a1:asc,a2:desc" in {
    PageRequestParsers.parseSort(Seq("a1:asc,a2:desc")) should be(Sort("a1" -> Some(Asc), "a2" -> Some(Desc)))
  }

  it should "process a1:Asc,a2:DESC" in {
    PageRequestParsers.parseSort(Seq("a1:Asc,a2:DESC")) should be(Sort("a1" -> Some(Asc), "a2" -> Some(Desc)))
  }

  it should "produce ParsingException for an empty String" in {
    intercept[ParsingException] {
      PageRequestParsers.parseSort(Seq(""))
    }
  }

  it should "produce ParsingException for a1:" in {
    intercept[ParsingException] {
      PageRequestParsers.parseSort(Seq("a1:"))
    }
  }

  it should "produce ParsingException for a1:x" in {
    intercept[ParsingException] {
      PageRequestParsers.parseSort(Seq("a1:x"))
    }
  }

  it should "produce ParsingException for a1," in {
    intercept[ParsingException] {
      PageRequestParsers.parseSort(Seq("a1,"))
    }
  }

  it should "produce ParsingException for a1:asc," in {
    intercept[ParsingException] {
      PageRequestParsers.parseSort(Seq("a1:asc,"))
    }
  }
}

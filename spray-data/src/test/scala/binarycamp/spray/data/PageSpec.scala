package binarycamp.spray.data

import org.scalatest.FlatSpec
import org.scalatest.Matchers._

class PageSpec extends FlatSpec with PageBehaviours {
  val items = Seq("1", "2", "3")
  val sort = Sort("p1" -> Some(Order.Asc), "p2" -> Some(Order.Desc), "p3" -> None)
  val filter = Filter("expression")

  "Page.apply(items)" should behave like
    pageWithEmptyItems(Page(Seq.empty[String]))

  "Page.apply(items={1,2,3})" should behave like
    page(Page(items), items, 0, 3, 3, false, false, None, None)

  "Page.apply(items, sort)" should behave like
    pageWithEmptyItems(Page(Seq.empty[String], sort))

  "Page.apply(items={1,2,3}, sort)" should behave like
    page(Page(items, sort), items, 0, 3, 3, false, false, Some(sort), None)

  "Page.apply(items, filter)" should behave like
    pageWithEmptyItems(Page(Seq.empty[String], filter))

  "Page.apply(items={1,2,3}, filter)" should behave like
    page(Page(items, filter), items, 0, 3, 3, false, false, None, Some(filter))

  "Page.apply(items, sort, filter)" should behave like
    pageWithEmptyItems(Page(Seq.empty[String], sort, filter))

  "Page.apply(items={1,2,3}, sort, filter)" should behave like
    page(Page(items, sort, filter), items, 0, 3, 3, false, false, Some(sort), Some(filter))

  "Page.apply(items, index, size)" should behave like
    pageWithEmptyItems(Page(Seq.empty[String], 0, 3))

  "Page.apply(items={1,2,3}, index=0, size=3)" should behave like
    pageWithSize(Page(items, 0, 3), items, 0, 3, 3, false, false, None, None)

  "Page.apply(items={1,2,3}, index=0, size=2)" should behave like
    pageWithSize(Page(items, 0, 2), items, 0, 2, Int.MaxValue, false, true, None, None)

  "Page.apply(items={1,2,3}, index=1, size=3)" should behave like
    pageWithSize(Page(items, 1, 3), items, 1, 3, 6, true, false, None, None)

  "Page.apply(items={1,2,3}, index=1, size=2)" should behave like
    pageWithSize(Page(items, 1, 2), items, 1, 2, Int.MaxValue, true, true, None, None)

  "Page.apply(items, index, size, sort)" should behave like
    pageWithEmptyItems(Page(Seq.empty[String], 0, 3, sort))

  "Page.apply(items={1,2,3}, index=0, size=3, sort)" should behave like
    pageWithSize(Page(items, 0, 3, sort), items, 0, 3, 3, false, false, Some(sort), None)

  "Page.apply(items, index, size, filter)" should behave like
    pageWithEmptyItems(Page(Seq.empty[String], 0, 3, filter))

  "Page.apply(items={1,2,3}, index=0, size=3, filter)" should behave like
    pageWithSize(Page(items, 0, 3, filter), items, 0, 3, 3, false, false, None, Some(filter))

  "Page.apply(items, index, size, sort, filter)" should behave like
    pageWithEmptyItems(Page(Seq.empty[String], 0, 3, sort, filter))

  "Page.apply(items={1,2,3}, index=0, size=3, sort, filter)" should behave like
    pageWithSize(Page(items, 0, 3, sort, filter), items, 0, 3, 3, false, false, Some(sort), Some(filter))

  "Page.apply(items, index, size, count)" should behave like
    pageWithEmptyItems(Page(Seq.empty[String], 0, 3, 3))

  "Page.apply(items={1,2,3}, index, size=2, count)" should behave like
    pageWithItemsOutOfBounds(Page(Seq.empty[String], 0, 2, 3))

  "Page.apply(items={1,2,3}, index=0, size=3, count=2)" should behave like
    pageWithInvalidCount(Page(Seq.empty[String], 0, 3, 2))

  "Page.apply(items={1,2,3}, index=0, size=3, count=3)" should behave like
    page(Page(items, 0, 3, 3), items, 0, 3, 3, false, false, None, None)

  "Page.apply(items={1,2,3}, index=1, size=3, count=6)" should behave like
    page(Page(items, 1, 3, 6), items, 1, 3, 6, true, false, None, None)

  "Page.apply(items={1,2,3}, index=1, size=3, count=7)" should behave like
    page(Page(items, 1, 3, 7), items, 1, 3, 7, true, true, None, None)

  "Page.apply(items, index, size, count, sort)" should behave like
    pageWithEmptyItems(Page(Seq.empty[String], 0, 3, 3, sort))

  "Page.apply(items={1,2,3}, index, size=2, count, sort)" should behave like
    pageWithItemsOutOfBounds(Page(Seq.empty[String], 0, 2, 3, sort))

  "Page.apply(items={1,2,3}, index=0, size=3, count=2, sort)" should behave like
    pageWithInvalidCount(Page(Seq.empty[String], 0, 3, 2, sort))

  "Page.apply(items={1,2,3}, index=0, size=3, count=3, sort)" should behave like
    page(Page(items, 0, 3, 3, sort), items, 0, 3, 3, false, false, Some(sort), None)

  "Page.apply(items, index, size, count, filter)" should behave like
    pageWithEmptyItems(Page(Seq.empty[String], 0, 3, 3, filter))

  "Page.apply(items={1,2,3}, index, size=2, count, filter)" should behave like
    pageWithItemsOutOfBounds(Page(Seq.empty[String], 0, 2, 3, filter))

  "Page.apply(items={1,2,3}, index=0, size=3, count=2, filter)" should behave like
    pageWithInvalidCount(Page(Seq.empty[String], 0, 3, 2, filter))

  "Page.apply(items={1,2,3}, index=0, size=3, count=3, filter)" should behave like
    page(Page(items, 0, 3, 3, filter), items, 0, 3, 3, false, false, None, Some(filter))

  "Page.apply(items, index, size, count, sort, filter)" should behave like
    pageWithEmptyItems(Page(Seq.empty[String], 0, 3, 3, sort, filter))

  "Page.apply(items={1,2,3}, index, size=2, count, sort, filter)" should behave like
    pageWithItemsOutOfBounds(Page(Seq.empty[String], 0, 2, 3, sort, filter))

  "Page.apply(items={1,2,3}, index=0, size=3, count=2, sort, filter)" should behave like
    pageWithInvalidCount(Page(Seq.empty[String], 0, 3, 2, sort, filter))

  "Page.apply(items={1,2,3}, index=0, size=3, count=3, sort, filter)" should behave like
    page(Page(items, 0, 3, 3, sort, filter), items, 0, 3, 3, false, false, Some(sort), Some(filter))
}

trait PageBehaviours {
  this: FlatSpec ⇒

  def page[T](page: ⇒ Page[T], items: Seq[T], index: Int, size: Int, count: Int, hasPrevious: Boolean,
              hasNext: Boolean, sort: Option[Sort], filter: Option[Filter]) = {
    val p = page
    it should "create a page with the provided list of items" in {
      p.items should be(items)
    }

    common(p, items, index, size, count, hasPrevious, hasNext, sort, filter)
  }

  def pageWithSize[T](page: ⇒ Page[T], items: Seq[T], index: Int, size: Int, count: Int, hasPrevious: Boolean,
                      hasNext: Boolean, sort: Option[Sort], filter: Option[Filter]) = {
    val p = page
    if (size < items.size) {
      it should s"create a page with first $size items when size < items.size" in {
        p.items should be(items.slice(0, size))
      }
    } else {
      it should "create a page with the provided list of items when size == items.size" in {
        p.items should be(items)
      }
    }

    common(p, items, index, size, count, hasPrevious, hasNext, sort, filter)
  }

  private def common[T](page: Page[T], items: Seq[T], index: Int, size: Int, count: Int, hasPrevious: Boolean,
                        hasNext: Boolean, sort: Option[Sort], filter: Option[Filter]) = {
    it should s"create a page with index ${index}" in {
      page.index should be(index)
    }

    it should s"create a page with size ${size}" in {
      page.size should be(size)
    }

    it should s"create a page with count ${if (count == Int.MaxValue) " == infinity" else count}" in {
      page.count should be(count)
    }

    it should (if (hasPrevious) "create a page with a previous page" else "create a page with no previous page") in {
      page.hasPrevious should be(hasPrevious)
    }

    it should (if (hasNext) "create a page with a next page" else "create a page with no next page") in {
      page.hasPrevious should be(hasPrevious)
    }

    it should (if (sort.isDefined) "create a page with sort" else "create a page with no sort") in {
      page.sort should be(sort)
    }

    it should (if (filter.isDefined) "create a page with filter" else "create a page with no filter") in {
      page.filter should be(filter)
    }
  }

  def pageWithEmptyItems[T](page: ⇒ Page[T]) =
    it should "produce an IllegalArgumentException when the list of items is empty" in {
      intercept[IllegalArgumentException] {
        page
      }
    }

  def pageWithItemsOutOfBounds[T](page: ⇒ Page[T]) =
    it should "produce an IllegalArgumentException when the list of items contains more than size items" in {
      intercept[IllegalArgumentException] {
        page
      }
    }

  def pageWithInvalidCount[T](page: ⇒ Page[T]) =
    it should "produce an IllegalArgumentException when index * size + items.size > count" in {
      intercept[IllegalArgumentException] {
        page
      }
    }
}

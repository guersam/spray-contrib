package binarycamp.spray.session

trait ToAttributeAccessorPimps {
  implicit def string2AttributeAccessor(name: String) = AttributeAccessor[String](name)
  implicit def symbol2AttributeAccessor(symbol: Symbol) = AttributeAccessor[String](symbol.name)
}

case class AttributeAccessor[A](name: String) {
  def set[B](value: B): AttributeSetter[B] = AttributeSetter[B](name, value)

  def set[B](value: B, serializer: ToStringSerializer[B]): AttributeSerializerSetter[B] =
    AttributeSerializerSetter[B](name, value, serializer)

  def remove: AttributeRemover = AttributeRemover(name)
}

case class AttributeSetter[A](name: String, value: A) {
  def serializer(serializer: ToStringSerializer[A]): AttributeSerializerSetter[A] =
    AttributeSerializerSetter[A](name, value, serializer)
}

case class AttributeSerializerSetter[A](name: String, value: A, serializer: ToStringSerializer[A])

case class AttributeRemover(name: String)

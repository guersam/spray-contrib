package binarycamp.spray.session

import com.typesafe.config.Config
import spray.http.Uri.Query

trait ContentCoder {
  def encode(session: Session): String
  def decode(content: String): Session
}

object ContentCoder {
  def apply(name: String, config: Option[Config]): ContentCoder = name match {
    case "plain-coder"        ⇒ PlainCoder
    case "javax-crypto-coder" ⇒ new JavaxCryptoCoderConfigurator(config).create
    case _                    ⇒ throw new IllegalArgumentException(s"Invalid ContentCoder $name")
  }
}

abstract class ContentCoderConfigurator(config: Option[Config]) {
  def create: ContentCoder
}

object PlainCoder extends ContentCoder {
  override def encode(session: Session): String = Query(session).toString()
  override def decode(content: String): Session = Query(content).toMap
}

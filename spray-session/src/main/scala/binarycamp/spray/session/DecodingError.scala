package binarycamp.spray.session

case class DecodingError(error: String, cause: Option[Throwable])

object DecodingError {
  def apply(error: String): DecodingError = DecodingError(error, None)

  def apply(error: String, cause: Throwable): DecodingError = DecodingError(error, Some(cause))
}

package binarycamp.spray.session

case class EncodingError(error: String, cause: Option[Throwable])

object EncodingError {
  def apply(error: String): EncodingError = new EncodingError(error, None)

  def apply(error: String, cause: Throwable): EncodingError = new EncodingError(error, Some(cause))
}

package binarycamp.spray.session

import shapeless._
import spray.routing._
import spray.routing.Directives._
import spray.routing.directives._

trait SessionDirectives extends CustomDeserializers with ToAttributeAccessorPimps {
  def session(magnet: SessionMagnet): magnet.Out = magnet()

  def setSession(magnet: SetSessionMagnet): Directive0 = magnet(true)

  def newSession(magnet: SetSessionMagnet): Directive0 = magnet(false)

  def removeSession(magnet: RemoveSessionMagnet): Directive0 = magnet()
}

object SessionDirectives extends SessionDirectives {
  private[session] def optionalSession(baker: CookieBaker): Directive1[Option[Session]] =
    optionalCookie(baker.name).flatMap {
      case Some(cookie) ⇒ baker.decode(cookie) match {
        case Right(data)                       ⇒ provide(Some(data))
        case Left(DecodingError(error, cause)) ⇒ reject(MalformedSessionRejection(error, cause))
      }
      case None ⇒ provide(None)
    }

  private[session] def setSession(data: Session, baker: CookieBaker): Directive0 =
    baker.encode(data) match {
      case Right(content)                    ⇒ setCookie(content)
      case Left(EncodingError(error, cause)) ⇒ reject(SessionEncodingFailedRejection(error, cause))
    }
}

trait SessionMagnet {
  type Out
  def apply(): Out
}

object SessionMagnet {
  implicit def apply[T](value: T)(implicit sm2: SessionMagnet2[T], baker: CookieBaker) =
    new SessionMagnet {
      override type Out = sm2.Out
      override def apply(): Out = sm2(value)
    }
}

trait SessionMagnet2[T] {
  type Out
  def apply(value: T): Out
}

object SessionMagnet2 {
  type Aux[A, B] = SessionMagnet2[A] { type Out = B }

  def Aux[A, B](f: A ⇒ B): Aux[A, B] = new SessionMagnet2[A] {
    override type Out = B
    override def apply(value: A): B = f(value)
  }

  import spray.httpx.unmarshalling.{ FromStringOptionDeserializer ⇒ FSOD, _ }

  private def filter[T](name: String, baker: CookieBaker, deserializer: FSOD[T]): Directive1[T] =
    SessionDirectives.optionalSession(baker).flatMap { session ⇒
      deserializer(session.flatMap(_.get(name))) match {
        case Right(data)                          ⇒ provide(data)
        case Left(MalformedContent(error, cause)) ⇒ reject(MalformedSessionAttributeRejection(name, error, cause))
        case Left(_)                              ⇒ reject(MissingSessionAttributeRejection(name))
      }
    }

  implicit def forString(implicit b: CookieBaker, d: FSOD[String]): Aux[String, Directive1[String]] =
    Aux[String, Directive1[String]] { name ⇒ filter[String](name, b, d) }

  implicit def forSymbol(implicit b: CookieBaker, d: FSOD[String]): Aux[Symbol, Directive1[String]] =
    Aux[Symbol, Directive1[String]] { symbol ⇒ filter[String](symbol.name, b, d) }

  implicit def forNameReceptacle[T](implicit b: CookieBaker, d: FSOD[T]): Aux[NameReceptacle[T], Directive1[T]] =
    Aux[NameReceptacle[T], Directive1[T]] { nr ⇒ filter[T](nr.name, b, d) }

  implicit def forNameDeserializerReceptacle[T](implicit b: CookieBaker): Aux[NameDeserializerReceptacle[T], Directive1[T]] =
    Aux[NameDeserializerReceptacle[T], Directive1[T]] { nr ⇒ filter[T](nr.name, b, nr.deserializer) }

  implicit def forNameDefaultReceptacle[T](implicit b: CookieBaker, d: FSOD[T]): Aux[NameDefaultReceptacle[T], Directive1[T]] =
    Aux[NameDefaultReceptacle[T], Directive1[T]] { nr ⇒ filter[T](nr.name, b, d.withDefaultValue(nr.default)) }

  implicit def forNameDeserializerDefaultReceptacle[T](implicit b: CookieBaker): Aux[NameDeserializerDefaultReceptacle[T], Directive1[T]] =
    Aux[NameDeserializerDefaultReceptacle[T], Directive1[T]] {
      nr ⇒ filter[T](nr.name, b, nr.deserializer.withDefaultValue(nr.default))
    }

  implicit def forTuple[T <: Product, L <: HList, Out](implicit hla: HListerAux[T, L], sm2: Aux[L, Out]): Aux[T, Out] =
    Aux[T, Out] { tuple ⇒ sm2(hla(tuple)) }

  implicit def forHList[L <: HList](implicit folder: LeftFolder[L, Directive0, MapReduce.type]): Aux[L, folder.Out] =
    Aux[L, folder.Out] { list ⇒ list.foldLeft(noop)(MapReduce) }

  object MapReduce extends Poly2 {
    implicit def forT[T, LA <: HList, LB <: HList, Out <: HList](implicit sm2: Aux[T, Directive[LB]],
                                                                 p: PrependAux[LA, LB, Out]) =
      at[Directive[LA], T]((a, t) ⇒ a & sm2(t))
  }
}

trait SetSessionMagnet {
  def apply(retain: Boolean): Directive0
}

object SetSessionMagnet {
  implicit def apply[T](t: T)(implicit baker: CookieBaker, ssm2: SetSessionMagnet2[T]): SetSessionMagnet =
    new SetSessionMagnet {
      override def apply(retain: Boolean): Directive0 = {
        def setSession(data: Session) = SessionDirectives.setSession(ssm2(t)(data), baker)
        if (retain) SessionDirectives.optionalSession(baker).flatMap { session ⇒ setSession(session.getOrElse(Map())) }
        else setSession(Map())
      }
    }
}

trait SetSessionMagnet2[T] {
  def apply(t: T): Session ⇒ Session
}

object SetSessionMagnet2 {
  type ST = Session ⇒ Session

  def apply[T](f: T ⇒ ST): SetSessionMagnet2[T] = new SetSessionMagnet2[T] {
    override def apply(t: T): ST = f(t)
  }

  implicit def forAttributeSetter[A](implicit s: ToStringSerializer[A]): SetSessionMagnet2[AttributeSetter[A]] =
    SetSessionMagnet2[AttributeSetter[A]] {
      setter ⇒ session ⇒ session.updated(setter.name, s(setter.value))
    }

  implicit def forAttributeSetterWithSerializer[A]: SetSessionMagnet2[AttributeSerializerSetter[A]] =
    SetSessionMagnet2[AttributeSerializerSetter[A]] {
      setter ⇒ session ⇒ session.updated(setter.name, setter.serializer(setter.value))
    }

  implicit def forAttributeRemover: SetSessionMagnet2[AttributeRemover] =
    SetSessionMagnet2[AttributeRemover] {
      remover ⇒ session ⇒ session - remover.name
    }

  implicit def forStringAnyPair[A](implicit s: ToStringSerializer[A]): SetSessionMagnet2[(String, A)] =
    SetSessionMagnet2[(String, A)] {
      case (name, value) ⇒ session ⇒ session.updated(name, s(value))
    }

  implicit def forSymbolAnyPair[A](implicit s: ToStringSerializer[A]): SetSessionMagnet2[(Symbol, A)] =
    SetSessionMagnet2[(Symbol, A)] {
      case (symbol, value) ⇒ session ⇒ session.updated(symbol.name, s(value))
    }

  implicit def forTuple[T <: Product, H <: HList](implicit hla: HListerAux[T, H],
                                                  ssm2: SetSessionMagnet2[H]): SetSessionMagnet2[T] =
    SetSessionMagnet2[T] { tuple ⇒ session ⇒ ssm2(hla(tuple))(session) }

  private val identity: ST = session ⇒ session

  implicit def forHList[L <: HList](implicit folder: LeftFolderAux[L, ST, Folder.type, ST]): SetSessionMagnet2[L] =
    SetSessionMagnet2[L] { list ⇒ session ⇒ folder(list, identity)(session) }

  object Folder extends Poly2 {
    implicit def forT[T](implicit ssm2: SetSessionMagnet2[T]) = at[ST, T] { (st, t) ⇒ st.andThen(ssm2(t)) }
  }
}

trait RemoveSessionMagnet {
  def apply(): Directive0
}

object RemoveSessionMagnet {
  implicit def fromUnit(u: Unit)(implicit baker: CookieBaker): RemoveSessionMagnet =
    new RemoveSessionMagnet {
      override def apply(): Directive0 = deleteCookie(baker.name)
    }
}

package binarycamp.spray.session

import akka.actor.ActorRefFactory
import binarycamp.spray.pimp._
import com.typesafe.config.Config
import spray.util._

case class SessionSettings(
  coderName: String,
  coderConfig: Option[Config],
  name: String,
  maxAge: Option[Long],
  domain: Option[String],
  path: Option[String],
  secure: Boolean,
  httpOnly: Boolean)

object SessionSettings extends SettingsCompanion[SessionSettings]("binarycamp.spray.session") {
  override def fromSubConfig(c: Config): SessionSettings = {
    val coderName = c.getString("coder-name")
    SessionSettings(
      coderName,
      c.getOptConfig(coderName),
      c.getString("name"),
      c.getOptLong("maxAge"),
      c.getOptString("domain"),
      c.getOptString("path"),
      c.getBoolean("secure"),
      c.getBoolean("httpOnly"))
  }

  implicit def default(implicit refFactory: ActorRefFactory): SessionSettings = apply(actorSystem)
}

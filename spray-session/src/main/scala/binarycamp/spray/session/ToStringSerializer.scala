package binarycamp.spray.session

import spray.http.Uri

trait ToStringSerializer[A] extends (A ⇒ String)

object ToStringSerializer {
  implicit val stringSerializer = new ToStringSerializer[String] {
    override def apply(value: String): String = value
  }

  implicit val intSerializer = new ToStringSerializer[Int] {
    override def apply(value: Int): String = value.toString
  }

  implicit val longSerializer = new ToStringSerializer[Long] {
    override def apply(value: Long): String = value.toString
  }

  implicit val uriSerializer = new ToStringSerializer[Uri] {
    override def apply(uri: Uri): String = uri.toString
  }
}

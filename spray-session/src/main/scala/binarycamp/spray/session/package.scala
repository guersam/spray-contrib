package binarycamp.spray

package object session {
  type Session = Map[String, String]
}
